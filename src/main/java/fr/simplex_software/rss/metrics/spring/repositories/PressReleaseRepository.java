package fr.simplex_software.rss.metrics.spring.repositories;

import fr.simplex_software.rss.metrics.spring.data.*;
import org.springframework.data.jpa.repository.*;

public interface PressReleaseRepository extends JpaRepository<PressReleaseEntity, Integer>
{
}
