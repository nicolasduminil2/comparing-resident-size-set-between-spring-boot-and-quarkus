package fr.simplex_software.rss.metrics.spring;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class RssApplication
{
  public static void main(String[] args)
  {
    SpringApplication.run(RssApplication.class, args);
  }
}
